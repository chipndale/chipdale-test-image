.PHONY: all volume pull build push run

all: volume build run

volume:
	docker volume create procoder-home
	#docker volume ls 
	#docker volume inspect procoder-home


build:
	docker build -t registry.gitlab.com/chipndale/chipdale-test-image:master .

run: 
	docker run --device /dev/dri -it --rm \
		--mount source=procoder-home,target=/home/procoder \
		-e DISPLAY=${DISPLAY} \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		 registry.gitlab.com/chipndale/chipdale-test-image:master \
		xterm

run_screen:
	docker run --device /dev/dri -it --rm \
		--mount source=procoder-home,target=/home/procoder \
		-e DISPLAY=${DISPLAY} \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		registry.gitlab.com/chipndale/chipdale-test-image:master  \
		xterm -e screen

pull: 
	docker pull  registry.gitlab.com/chipndale/test-image:master 

push: 
	docker push registry.gitlab.com/chipndale/chipdale-test-image:master 

help:
	@echo "Available targets:"
	 @echo "    volume : creates procoder-home Docker volume"
	 @echo "    build"
	 @echo "    run"
	 @echo "    pull"
	 @echo "    push"
	 @echo "    all : volume build run"

