FROM fedora:38
# Needed for mosaic_BAG documentation CI builds
RUN  dnf -y install tcsh make git python3 python3-pip python3-sphinx
RUN pip3 install sphinx-rtd-theme
RUN pip3 install myst_parser

